# Hochschild (co)homology

This repository contains unofficial notes for the lecture course _Hochschild (co)homology_ that is given by Dr. Pieter Belmans at the University of Bonn in the summer term of 2021.
A compiled version of these notes is available at [https://lecture-notes-bonn.gitlab.io/original/hochschild-cohomology-notes-ss21/hochschild-2021.pdf][1].

Feel free to contact us for comments or corrections.

[1]:https://lecture-notes-bonn.gitlab.io/original/hochschild-cohomology-notes-ss21/hochschild-2021.pdf 
