\chapter{Hochschild (co)homology for algebras}





\section{Definition}



\subsection{(Co)chain complexes}


\begin{convention}
	In the following,~$\kf$\glsadd{ground field} is a field.
	By an \defemph{algebra}\index{algebra} we mean an associative, unital~\algebra{$\kf$}.
\end{convention}


\begin{remark}
	The two main types of algebras to keep in mind are finite-dimensional algebras (which are not necessarily commutative) and commutative algebras (which are not necessarily finite-dimensional).
\end{remark}


\begin{recall}
	Let~$A$ and~$B$ be two algebras.
	\begin{enumerate}
		\item
			An~\bimodule{$A$}[$B$]\index{bimodule} is a vector space~$M$ together with the structure of a left~\module{$A$} and the structure of a right~\module{$B$}.
			These structures need to be compatible, in the sense that for all~$a \in A$,~$b \in B$, and~$m \in M$,
			\[
				(a \cdot m) \cdot b
				=
				a \cdot (m \cdot b) \,.
			\]
		\item
			An~\bimodule{$A$}[$B$] is the same as an~\module{$(A \tensor B^{\op})$}.
			These two kind of module structures on a vector space~$M$ are for all~$a \in A$,~$b \in B$, and~$m \in M$ related via the formula
			\[
				( a \tensor b^{\op} ) \cdot m
				=
				a \cdot m \cdot b \,.
			\]
		\item
			The term~\enquote{\bimodule{$A$}[$A$]} is abbreviated as~\enquote{\bimodule{$A$}}\index{bimodule}.
		\item
			The \defemph{enveloping algebra}\index{enveloping algebra} of~$A$ is the algebra
			\[
				\Ae
				\defines
				A \tensor A^{\op} \,.
				\glsadd{enveloping algebra}
			\]
		\item
			We have an isomorphism of algebras given by
			\[
				( \Ae )^{\op}
				\cong
				( A \tensor A^{\op} )^{\op}
				\cong
				A^{\op} \tensor ( A^{\op} )^{\op}
				\cong
				A^{\op} \tensor A
				\cong
				A \tensor A^{\op}
				=
				\Ae \,.
			\]
			This isomorphism is more explicitely given for all~$a_0, a_1 \in A$ by
			\[
				( \Ae )^{\op}
				\to
				\Ae \,,
				\quad
				\bigl( a_0 \tensor a_1^{\op} \bigr)^{\op}
				\mapsto
				a_1 \tensor a_0^{\op} \,.
			\]
		\item
			For a vector space~$M$, the following additional structures on~$M$ are equivalent.
			\begin{equivalenceslist}
				\item
					An~\bimodule{$A$} structure on~$M$.
				\item
					A left~\module{$\Ae$} structure on~$M$.
				\item
					A right~\module{$\Ae$} structure on~$M$.
			\end{equivalenceslist}
			These three kinds of module structures are for all~$a_0, a_1 \in A$ and~$m \in M$ related via the equalities
			\[
				(a_0 \tensor a_1^{\op}) \cdot m
				\quad
				=
				\quad
				a_0 \cdot m \cdot a_1
				\quad
				=
				\quad
				m \cdot (a_1 \tensor a_0^{\op}) \,.
			\]
		\item
			Let~$M$ and~$N$ be two~\bimodules{$A$}.
			We can regard~$M$ as a right~\module{$A$} and~$N$ as a left~\module{$A$}, and then form their tensor product~$M \tensor_{\Ae} N$.
			We can simililary regard~$M$ as a left~\module{$A$} and~$N$ as a right~\module{$A$}, and then form their tensor product~$N \tensor_{\Ae} M$.
			We have more explicitely
			\begin{align*}
				M \tensor_{\Ae} N
				&\cong
				( M \tensor_{\kf} N )
				\big/
				\gen*{
					\bigl( m \cdot (a_0 \tensor a_1^{\op}) \bigr) \tensor n
					-
					m \tensor \bigl( (a_0 \tensor a_1^{\op}) \cdot n \bigr)
				\suchthat*
					a_1, a_2 \in A
				} \,,
			\intertext{and similarly}
				N \tensor_{\Ae} M
				&\cong
				( N \tensor_{\kf} M )
				\big/
				\gen*{
					\bigl( n \cdot (a_0 \tensor a_1^{\op}) \bigr) \tensor m
					-
					n \tensor \bigl( (a_0 \tensor a_1^{\op}) \cdot m \bigr)
				\suchthat*
					a_0, a_1 \in A
				} \,,
			\end{align*}
			By rewriting the above expressions as
			\begin{align*}
				M \tensor_{\Ae} N
				&\cong
				( M \tensor_{\kf} N )
				\big/
				\gen*{
					( a_1 \cdot m \cdot a_0 ) \tensor n
					-
					m \tensor (a_0 \cdot n \cdot a_1 )
				\suchthat*
					a_0, a_1 \in A
				} \,,
			\intertext{and similarly}
				N \tensor_{\Ae} M
				&\cong
				( N \tensor_{\kf} M )
				\big/
				\gen*{
					( a_1 \cdot n \cdot a_0 ) \tensor m
					-
					n \tensor  (a_0 \cdot m \cdot a_1)
				\suchthat*
					a_0, a_1 \in A
				}
			\end{align*}
			we see that the symmetry isomorphism of vector spaces
			\begin{alignat*}{2}
				M \tensor_{\kf} N
				&\to
				N \tensor_{\kf} M \,,
				&\quad
				m \tensor n
				&\mapsto
				n \tensor m
			\intertext{induces in isomorphism of vector spaces}
				M \tensor_{\Ae} N
				&\to
				N \tensor_{\Ae} M \,,
				&\quad
				m \tensor n
				&\mapsto
				n \tensor m \,.
			\end{alignat*}
			We therefore don’t need to distinguish between the two choices of tensor product~$M \tensor_{\Ae} N$ and~$N \tensor_{\Ae} M$.
	\end{enumerate}
\end{recall}


\begin{construction}
	Let~$A$ be a~\algebra{$\kf$}.
	\begin{enumerate}
		\item
			We can regard the bilinear multiplication of~$A$ as a linear map
			\[
				m
				\colon
				A \tensor A
				\to
				A \,,
				\quad
				a_0 \tensor a_1
				\mapsto
				a_0 a_1 \,.
			\]
			This map is surjective since~$A$ in unital.
			More explicitely, any element~$a_0$ of~$A$ admits the preimage
			\begin{equation}
				\label{preimage under multiplication}
				1 \tensor a_0 \,.
			\end{equation}
			The following sequence is therefore exact:
			\[
				A \tensor A
				\xto{m}
				A
				\to
				0 \,.
			\]
		\item
			We want to extend this exact sequence by describing the kernel of~$m$.
			The associativity of~$A$ tells us that all elements of the form
			\[
				a_0 b \tensor a_1 - a_0 \tensor b a_1
			\]
			with~$a_0, b, a_1 \in A$ are contained in the kernel of~$A$.
			In other words, the sequence
			\begin{equation}
				\label{restricted bar resolution}
				A \tensor A \tensor A
				\xto{d_1}
				A \tensor A
				\xto{m}
				A
				\to
				0
			\end{equation}
			in a chain complex, where the linear map~$d_1$ is given by
			\[
				d_1
				\colon
				A \tensor A \tensor A
				\to
				A \tensor A \,,
				\quad
				a_0 \tensor a_1 \tensor a_2
				\mapsto
				a_0 a_1 \tensor a_2 - a_0 \tensor a_1 a_2 \,.
			\]
			We note that the map~$d_1$ can equivalently be described as
			\[
				d_1
				=
				m \tensor \id - \id \tensor m \,.
			\]
			We can now see that the chain complex~\eqref{restricted bar resolution} is exact at~$A \tensor A$.
			Motivated by~\eqref{preimage under multiplication} we consider for every element~$t$ of the kernel of~$m$ the element
			\[
				1 \tensor t
			\]
			of~$A \tensor A \tensor A$.
			We note that~$(m \tensor \id)(1 \tensor t) = t$ and therefore
			\[
				d_1(1 \tensor t)
				=
				(m \tensor \id - \id \tensor m)(1 \tensor t)
				=
				t - 1 \tensor m(t)
				=
				t - 0
				=
				t \,.
			\]
			This shows that the sequence~\eqref{restricted bar resolution} is indeed exact at~$A \tensor A$, and therefore exact.
		\item
			Looking at the above calculations, we see that they can be generalized as follows:
			We set
			\[
				C_n
				\defined
				A^{\tensor (n+2)}
			\]
			for every~$n \geq -1$, and write~$\id_n$ for~$\id_{C_n}$.
			We recursively define linear maps
			\[
				d_n
				\colon
				C_n
				\to
				C_{n-1}
			\]
			for every~$n \geq 0$ via
			\begin{equation}
				\label{recursive definition of bar differential}
				d_0
				\defined
				m
				\quad\text{and}\quad
				d_{n+1}
				\defines
				m \tensor \id_n - \id_A \tensor d_n \,.
			\end{equation}
			We claim that the resulting sequence
			\begin{equation}
				\label{bar resolution}
				\dotsb
				\to
				C_n
				\xto{d_n}
				C_{n-1}
				\xto{d_{n-1}}
				C_{n-2}
				\to
				\dotsb
				\to
				C_1
				\xto{d_1}
				C_0
				\xto{d_0}
				C_{-1}
				\to
				0
			\end{equation}
			is exact.
	\item
			We first note that the solution to the recursive formula~\eqref{recursive definition of bar differential} is explicitely given by
			\[
				d_n
				=
				\sum_{i=0}^n
				(-1)^i
				\underbrace{ \id_A \tensor \dotsb \tensor \id_A }_{i}
				{} \tensor m \tensor {} % underbrace and tensor don’t interact well in terms of spacing
				\underbrace{ \id_A \tensor \dotsb \tensor \id_A }_{n-i} \,,
			\]
			Even more explicitely,
			\begin{equation}
				\label{explicit formula for bar differential}
				d_n(a_0 \tensor \dotsb \tensor a_{n+1})
				=
				\sum_{i=0}^n
				(-1)^i
				a_0 \tensor \dotsb \tensor a_i a_{i+1} \tensor \dotsb \tensor a_{n+1}
			\end{equation}
			for all~$a_0, \dotsc, a_{n+1} \in A$.
			To show that the sequence~\eqref{bar resolution} is a chain complex, we note that the formula~\eqref{explicit formula for bar differential} is compatible with the action of~$A$ on the first tensor factor via left multiplication.
			More abstractly speaking,
			\begin{equation}
				\label{technical formula for induction step}
				(m \tensor \id_{n-1}) \circ (\id_A \tensor d_n)
				=
				d_n \circ (m \tensor \id_n) \,.
			\end{equation}

			We have previously seen that~$d_0 \circ d_1 = 0$ because~$A$ is associative.
			We also find with the help of~\eqref{technical formula for induction step} that for every~$n \geq 0$,
			\begin{align*}
				d_{n+1} \circ d_{n+2}
				={}&
				( m \tensor \id_n - \id_A \tensor d_n )
				\circ
				( m \tensor \id_{n+1} - \id_A \tensor d_{n+1} )
				\\
				={}&
				(m \tensor \id_n) \circ (m \tensor \id_{n+1})
				- (m \tensor \id_n) \circ (\id_A \tensor d_{n+1})
				\\
				{}&
				- (\id_A \tensor d_n) \circ (m \tensor \id_{n+1})
				+ (\id_A \tensor d_n) \circ (\id_A \tensor d_{n+1})
				\\
				={}&
				(m \tensor \id_n) \circ (m \tensor \id_{n+1})
				- d_{n+1} \circ ( m \tensor \id_{n+1} )
				\\
				{}&
				- (\id_A \tensor d_n) \circ (m \tensor \id_{n+1})
				+ (\id_A \tensor d_n) \circ (\id_A \tensor d_{n+1})
				\\
				={}&
				\underbrace{ (m \tensor \id_n - \id_A \tensor d_n - d_{n+1}) }_{= 0}
				{}
				\circ (m \tensor \id_{n+1})
				+ \id_A \tensor (d_n \circ d_{n+1})
				\\
				={}&
				\id_A \tensor (d_n \circ d_{n+1}) \,.
			\end{align*}
			It therefore follows by induction that~$d_n \circ d_{n+1} = 0$ for every~$n \geq 0$, which shows that the sequence~\eqref{bar resolution} is indeed a chain complex.

			To show the exactness of the chain complex~\eqref{bar resolution} we use the same approach as before:
			for every~$n \geq 0$ and every element~$t$ in the kernel of~$d_n$ we consider the element~$1 \tensor t$ of~$C_{n+1}$.
			We have
			\[
				d_{n+1}(1 \tensor t)
				=
				(m \tensor \id_n - \id_A \tensor d_n)(1 \tensor t)
				=
				t - 1 \tensor d_n(t)
				=
				t - 1 \tensor 0
				=
				t \,.
			\]
			This shows that~$t$ is contained in the image of~$d_{n+1}$.%
			\footnote{
				We kave choosen the differential~$d_n$ precisely so that this calculation goes trough.
			}

			We have overall seen that the sequence~\eqref{bar resolution} is indeed exact.
		\item
			The tensor power~$C_n = A^{\tensor (n + 2)}$ admits for every exponent~$n \geq -1$ the  additional structure of an~\bimodule{$A$} via
			\[
				b \cdot (a_0 \tensor \dotsb \tensor a_n) \cdot c
				\defined
				(b a_0) \tensor a_1 \tensor \dotsb \tensor a_{n-1} \tensor (a_n c) \,.
			\]
			for all~$b, c \in A$ and~$a_0, \dotsc, a_{n+1} \in A$.%
			\footnote{
				For~$C_{-1} = A^{\tensor 1} = A$, this formula is to be read as~$b \cdot a_0 \cdot c = b a_0 c$.
			}
			The~\linear{$\kf$} maps~$d_n$ are already homomorphisms of~\bimodules{$A$}.%
			\footnote{
				We have already expressed in~\eqref{technical formula for induction step} that they are homomorphisms of left~\modules{$A$}.
			}
			The short exact sequence of vector spaces~\eqref{bar resolution} is therefore a short exact sequence of~\bimodules{$A$}.
			
			We can regard the~\bimodules{$A$}~$C_n$ equivalently as left~\modules{$\Ae$}.
			We then have for every~$n \geq 0$ an isomorphism of~\modules{$\Ae$} given by
			\[
				\Ae \tensor_{\kf} A^{\tensor n}
				\to
				C_n \,,
				\quad
				( a_0 \tensor a_{n+1}^{\op} ) \tensor ( a_1 \tensor \dotsb \tensor a_n )
				\mapsto
				a_0 \tensor a_1 \tensor \dotsb \tensor a_n \tensor a_{n+1}
			\]
			It follows for every~$n \geq 0$ that~$C_n$ is free as an~\module{$\Ae$}, because~$A^{\tensor n}$ is free as a~\module{$\kf$}.
		\item
			We have overall constructed a free resolution of~$A$ as an~\module{$\Ae$}.
			We will give this resolution a special name.
	\end{enumerate}
\end{construction}


\begin{definition}
	Let~$A$ be an algebra.
	For every~$n \geq 0$ let
	\[
		\Chain_n(A)
		\defined
		A^{\tensor (n+2)} \,.
	\]
	For every~$n \geq 1$ let~$d_n$ be the linear map given by
	\[
		d_n
		\colon
		\Chain_n(A)
		\to
		\Chain_{n-1}(A) ,,
		\quad
		a_0 \tensor \dotsb \tensor a_{n+1}
		\mapsto
		\sum_{i=0}^n
		(-1)^i
		a_0 \tensor \dotsb \tensor a_i a_{i+1} \tensor \dotsb \tensor a_{n+1} \,,
	\]
	and let~$d_0$ be the linear map given by
	\[
		d_0
		\colon
		C_0
		\to
		A \,,
		\quad
		a_0 \tensor a_1
		\mapsto
		a_0 a_1 \,.
	\]
	The chain complex
	\[
		\dotsb
		\to
		\Chain_3(A)
		\xto{d_3}
		\Chain_2(A)
		\xto{d_2}
		\Chain_1(A)
		\xto{d_1}
		\Chain_0(A)
	\]
	is the \defemph{bar complex}\index{bar complex} of~$A$.
	It is denoted by~$\Chain_\bullet(A)$.\glsadd{bar complex}
\end{definition}


\begin{warning}
	The algebra~$A$ itself is not part of the bar complex!
\end{warning}


\begin{remark}
	The word \enquote{bar} in the name \enquote{bar complex} refers to some old notation that we won’t be using in this course.
\end{remark}


\begin{proposition}
	Let~$A$ be an algebra.
	The sequence
	\[
		\Chain_\bullet(A)
		\xto{d_0}
		A
		\to
		0
	\]
	is a free resolution of~$A$ as an~\module{$\Ae$}.
	\qed
\end{proposition}


\begin{definition}
	Let~$A$ be an algebra and let~$M$ be an~\bimodule{$A$}.
	\begin{enumerate}
		\item
			The \defemph{Hochschild chain complex}\index{Hochschild!chain complex} of~$A$ with \defemph{coefficients in~$M$} is the chain complex of vector spaces
			\[
				\Chain_\bullet(A, M)
				\defined
				M \tensor_{\Ae} \Chain_\bullet(A) \,.
				\glsadd{hoch chain coeff}
			\]
		\item
			The \defemph{Hochschild cochain complex}\index{Hochschild!cochain complex} of~$A$ with \defemph{coefficients in~$M$} is the cochain complex of vector spaces
			\[
				\Chain^\bullet(A, M)
				\defined
				\Hom_{\Ae}\bigl( \Chain_\bullet(A), M \bigr) \,.
				\glsadd{hoch cochain coeff}
			\]
		\item
			The \defemph{Hochschild homology}\index{Hochschild!homology} of~$A$ with \defemph{coefficients in~$M$} is the homology of the corresponding Hochschild chain complex~$\Chain_\bullet(A, M)$, and it is denoted by~$\HH_\bullet(A, M)$.
			More explicitely,
			\[
				\HH_\bullet(A, M)
				\defined
				\Homology_\bullet\bigl( \Chain_\bullet(A, M) \bigr) \,.
				\glsadd{hoch homology coeff}
			\]
		\item
			The \defemph{Hochschild cohomology}\index{Hochschild!cohomology} of~$A$ with \defemph{coefficients in~$M$} is the cohomology of the corresponding Hochschild cochain complex~$\Chain^\bullet(A, M)$, and it is denoted by~$\HH^\bullet(A, M)$.
			More explicitely,
			\[
				\HH^\bullet(A, M)
				\defined
				\Homology^\bullet\bigl( \Chain^\bullet(A, M) \bigr) \,.
				\glsadd{hoch cohomology coeff}
			\]
		\item
			The \defemph{Hochschild homology}\index{Hochschild!homology} of~$A$ is~$\HH_\bullet(A) \defined \HH_\bullet(A, A)$.\glsadd{hoch homology}
		\item
			The \defemph{Hochschild cohomology}\index{Hochschild!cohomology} of~$A$ is~$\HH^\bullet(A) \defined \HH^\bullet(A, A)$.\glsadd{hoch cohomology}
	\end{enumerate}
\end{definition}


\subsection{Homological interpretation}


\begin{proposition}
	\label{hh is morite equivalence}
	Let~$A$ be an algebra and let~$M$ be an~\bimodule{$A$}.
	Then
	\[
		\HH^\bullet(A, M)
		\cong
		\Ext_{\Ae}^\bullet(A, M)
		\qquad\text{and}\qquad
		\HH_\bullet(A, M)
		\cong
		\Tor^{\Ae}_\bullet(A, M) \,.
	\]
\end{proposition}


\begin{proof}
	We know that the bar complex~$\Chain_\bullet(A)$ is a free resolution of~$A$ as an~\module{$\Ae$}.
	By describing~$\Ext_{\Ae}^{\bullet}(\ph, M)$ as the right derived functor(s) of~$\Hom_{\Ae}(\ph, M)$, it now follow that
	\begin{align*}
		\Ext_{\Ae}^\bullet(A, M)
		&\cong
		\bigl( \Right^\bullet \Hom_{\Ae}(\ph, M) \bigr)(A)
		\\
		&\cong
		\Homology^\bullet\bigl( \Hom_{\Ae}(\ph, M)\bigl( \Chain_\bullet(A) \bigr) \bigr)
		\\
		&=
		\Homology^\bullet\bigl( \Hom_{\Ae}(\Chain_\bullet(A), M) \bigr)
		\\
		&=
		\Homology^\bullet( \Chain^\bullet(A, M) )
		\\
		&=
		\HH^\bullet(A, M) \,.
	\end{align*}
	We can similarly describe~$\Tor^{\Ae}_\bullet(\ph, M)$ as the left derived functor(s) of~$(\ph) \tensor_{\Ae} M$.
	We find in this way, that
	\begin{align*}
		\SwapAboveDisplaySkip
		\Tor^{\Ae}_\bullet(A, M)
		&\cong
		\bigl( \Left_\bullet \bigl( (\ph) \tensor_{\Ae} M \bigr) \bigr)(A)
		\\
		&\cong
		\Homology_\bullet\bigl( \bigl( (\ph) \tensor_{\Ae} M \bigr)( \Chain_\bullet(A) ) \bigr)
		\\
		&=
		\Homology_\bullet\bigl( \Chain_\bullet(A) \tensor_{\Ae} M \bigr)
		\\
		&=
		\Homology_\bullet\bigl( M \tensor_{\Ae} \Chain_\bullet(A) \bigr)
		\\
		&=
		\Homology_\bullet( \Chain_\bullet(A, M) )
		\\
		&=
		\HH_\bullet(A, M) \,.
	\end{align*}
	This proves the claimed isomorphisms.
\end{proof}


\begin{remark}
	The reader may be wondering why we haven’t just defined the Hochschild (co)homology of an algebra via~$\Ext$ and~$\Tor$ in the first please, without wasting time on the Hochschild (co)chain complexes.
	We assure the reader that this question will be answered in time.%
	\footnote{
		Where \enquote{time} in roughly one week away.
	}

	We will need the Hochschild (co)chain complexes to give explicit interpretions of Hochschild (co)homology, and to understand the additional structure(s) that Hochschild cohomoloy carries.
	(It is a topic of ongoing research how far these additional structures depend on the choice of resolutions.)
\end{remark}


\begin{remark}
	It follows from~\cref{hh is morite equivalence} that the Hochschild (co)homology of an algebra is a Morita invariant\index{Morita invariant}, in the sense that the Hochschild (co)homology of~$A$ depends only on the category of~\bimodules{$A$}.
\end{remark}



\subsection{Reinterpretation}


\begin{fluff}
	We will now replace the Hochschild (co)chain complex by isomorphic (co)chain complexes that will be easier to work with.
\end{fluff}


\begin{construction}
	Let~$A$ be an algebra and let~$M$ be an~\bimodule{$A$}.
	\begin{enumerate}
		\item
			We have for every~$n \geq 0$ an isomorphism of vector spaces
			\begin{align*}
				\Chain^n(A, M)
				&=
				\Hom_{\Ae}(\Chain_n(A), M)
				\\
				&=
				\Hom_{\Ae}(A^{\tensor (n+2)}, M)
				\\
				&\cong
				\Hom_{\Ae}(\Ae \tensor_{\kf} A^{\tensor n}, M)
				\\
				&\cong
				\Hom_{\kf}(A^{\tensor n}, M) \,.
			\end{align*}
			We denote this isomorphism by~$\Phi$.
			For any element~$F$ of~$\Hom_{\Ae}(\Chain_n(A), M)$, the corresponding element~$f = \Phi(F)$ of~$\Hom_{\kf}(A^{\tensor{n}}, M)$ is given for all~$a_1, \dotsc, a_n \in A$ by
			\[
				f(a_1 \tensor \dotsb \tensor a_n)
				=
				F(1 \tensor a_1 \tensor \dotsb \tensor a_n \tensor 1)
			\]
			Conversely, for any element~$g$ of~$\Hom_{\kf}(A^{\tensor n}, M)$ the corresponding element~$G = \Phi^{-1}(g)$ of~$\Hom_{\Ae}(\Chain_n(A), M)$ is given for all~$a_0, \dotsc, a_{n+1} \in A$ by
			\[
				G(a_0 \tensor \dotsb \tensor a_{n+1})
				=
				a_0 \cdot g(a_1 \tensor \dotsb \tensor a_n) \cdot a_{n+1} \,.
			\]
			
			The differential from~$\Chain_n(A, M)$ to~$\Chain_{n+1}(A, M)$ corresponds under the isomorphism~$\Phi$ to a linear map
			\[
				d^n
				\colon
				\Hom_{\kf}(A^{\tensor n}, M)
				\to
				\Hom_{\kf}(A^{\tensor (n+1)}, M) \,,
			\]
			To better understand this linear map~$d^n$, let
			\[
				d_{n+1}
				\colon
				\Chain_{n+1}(A)
				\to
				\Chain_n(A)
			\]
			be the differential of the bar complex.
			The induced differential of~$\Chain^\bullet(A, M)$ is given by the dual map of~$d_{n+1}$, i.e.
			\[
				d_{n+1}^*
				\colon
				\Chain^n(A, M)
				\to
				\Chain^{n+1}(A, M) \,.
			\]
			For every element~$f$ of~$\Hom_{\kf}(A^{\tensor n}, M)$ we now have
			\[
				d^n(f)
				=
				\Phi( d_{n+1}^*( \Phi^{-1}(f) ) )
				=
				\Phi( \Phi^{-1}(f) \circ d_{n+1} ) \,,
			\]
			and therefore
			\begin{align*}
				{}&
				d^n(f)(a_1 \tensor \dotsb \tensor a_{n+1})
				\\
				={}&
				\Phi( \Phi^{-1}(f) \circ d_{n+1} )(a_1 \tensor \dotsb \tensor a_{n+1})
				\\
				={}&
				(\Phi^{-1}(f) \circ d_{n+1}) (1 \tensor a_1 \tensor \dotsb \tensor a_{n+1} \tensor 1)
				\\
				={}&
				\Phi^{-1}(f)( d_{n+1}(1 \tensor a_1 \tensor \dotsb \tensor a_{n+1} \tensor 1) )
				\\
				={}&
				\Phi^{-1}(f)
				\biggl(
					a_1 \tensor \dotsb \tensor a_{n+1} \tensor 1
					+ \sum_{i=1}^n
					(-1)^i
					1 \tensor a_1 \tensor \dotsb \tensor a_i a_{i+1} \tensor \dotsb \tensor a_{n+1} \tensor 1
				\\
				{}&
				\phantom{
					\Phi^{-1}(f)
					\biggl(
				}
				+ (-1)^{n+1} 1 \tensor a_1 \tensor \dotsb \tensor a_{n+1}
				\biggr)
				\\
				={}&
				a_1 f(a_2 \tensor \dotsb \tensor a_{n+1})
				+ \sum_{i=1}^n (-1)^i f(a_1 \tensor \dotsb \tensor a_i a_{i+1} \tensor \dotsb a_{n+1})
				\\
				{}&
				+ (-1)^{n+1} f(a_1 \tensor \dotsb \tensor a_n) a_{n+1} \,.
			\end{align*}
		\item
			We have for every~$n \geq 0$ an isomorphism of vector spaces
			\begin{align*}
				\Chain_n(A, M)
				&=
				M \tensor_{\Ae} \Chain_n(A)
				\\
				&\cong
				M \tensor_{\Ae} A^{\tensor (n+2)}
				\\
				&\cong
				M \tensor_{\Ae} \Ae \tensor_{\kf} A^{\tensor n}
				\\
				&\cong
				M \tensor_{\kf} A^{\tensor n} \,.
			\end{align*}
			We denote this isomorphism by~$\Psi$.
			This isomorphism is for all~$m \in M$ and~$a_0, \dotsc, a_{n+1} \in A$ given by
			\[
				\Psi(m \tensor a_0 \tensor \dotsb \tensor a_{n+1})
				=
				(a_{n+1} \cdot m \cdot a_0) \tensor a_1 \tensor \dotsb \tensor a_n \,,
			\]
			and its inverse is for all~$m \in M$ and~$a_1, \dotsc, a_n \in A$ given by
			\[
				\Psi^{-1}(m \tensor a_1 \tensor \dotsb \tensor a_n)
				=
				m \tensor (1 \tensor a_1 \tensor \dotsb \tensor a_n \tensor 1) \,.
			\]
			
			The differential from~$\Chain_n(A, M)$ to~$\Chain_{n-1}(A, M)$ corresponds under the isomorphism~$\Psi$ to a linear map
			\[
				d_n
				\colon
				M \tensor_{\kf} A^{\tensor n}
				\to
				M \tensor_{\kf} A^{\tensor (n-1)} \,.
			\]
			To better express this linear map, let
			\[
				d_n
				\colon
				\Chain_n(A)
				\to
				\Chain_{n-1}(A)
			\]
			be the differential of the bar complex.
			(Note the abuse of notation.)
			The induced differential of the Hochschild chain complex~$\Chain_\bullet(A, M)$ is given by the induced linear map
			\[
				\id_M \tensor d_n
				\colon
				\Chain_n(A, M)
				\to
				\Chain_{n-1}(A, M) \,.
			\]
			We find that
			\begin{align*}
				{}&
				d_n(m \tensor a_1 \tensor \dotsb \tensor a_n)
				\\
				={}&
				\Psi( (\id_M \tensor d_n)( \Psi^{-1}(m \tensor a_1 \tensor \dotsb \tensor a_n) ) )
				\\
				={}&
				\Psi( (\id_M \tensor d_n)( m \tensor 1 \tensor a_1 \tensor a_n \tensor 1) )
				\\
				={}&
				\Psi( m \tensor d_n(1 \tensor a_1 \tensor \dotsb \tensor a_n \tensor 1) )
				\\
				={}&
				\Psi \biggl(
					m \tensor a_1 \tensor \dotsb \tensor a_n \tensor 1
					+ \sum_{i=1}^n
					(-1)^i
					m \tensor 1 \tensor \dotsb \tensor a_i a_{i+1} \tensor \dotsb \tensor 1
				\\
				{}&
				\phantom{
					\Psi \biggl(
				}
					+ (-1)^n m \tensor 1 \tensor a_1 \tensor \dotsb \tensor a_n
				\biggr)
				\\
				={}&
				(m \cdot a_1) \tensor a_2 \tensor \dotsb \tensor a_n
				+ \sum_{i=1}^n
				(-1)^i
				m \tensor a_1 \tensor \dotsb \tensor a_i a_{i+1} \tensor \dotsb \tensor a_n
				\\
				{}&
				+ (-1)^n (a_n \cdot m) \tensor a_1 \tensor \dotsb \tensor a_{n-1} \,.
			\end{align*}
	\end{enumerate}
\end{construction}


\begin{fluff}
	Let~$A$ be an algebra and let~$M$ be an~\bimodule{$A$}.
	To better compute the Hochschild (co)homology of~$A$ with coefficients in~$M$, we can now replace the Hochschild cochain complex~$\Chain^\bullet(A, M)$ with the isomorphic cochain complex~$\Hom_{\kf}( A^{\tensor \bullet}, M )$, and we can replace the Hochschild chain complex~$\Chain_\bullet(A, M)$ with the isomorphic chain complex~$M \tensor_{\kf} A^{\tensor \bullet}$.
\end{fluff}


\begin{remark}
	Let~$A$ be an algebra and let~$M$ be an~\bimodule{$A$}.
	The above formula for the differential of~$M \tensor_{\kf} A^{\tensor \bullet}$ may be graphically expressed as follows:

	Let~$m$ be an element of~$M$ and let~$a_1, \dotsc, a_n$ be elements of~$A$.
	We arrange these elements in a circle as depicted in \cref{cyclic hochschild}.
	\begin{figure}
		\centering
		\begin{tikzpicture}
			\foreach \a in {0, 45, ..., 315} {
				\draw (\a : 2) -- (\a + 45 : 2);
			}
			\foreach \a in {0, 45, ..., 315} {
				\draw[fill=white,white] (\a : 2) circle (0.4);
			}

			\draw (90 : 2) node {$m$};
			\draw (135 : 2) node {$a_n$};
			\draw (180 : 2) node {$\hspace{1ex}\vdots$}; % sorry
			\foreach \i in {1, 2, ..., 5} {
				\draw (90 - 45 * \i: 2) node {$a_{\i}$};
			}
		\end{tikzpicture}
		\caption{Putting elements in a circle}
		\label{cyclic hochschild}
	\end{figure}
	It this circle, there are~$n + 2$ ways of multiplying two adjacent elements.
	By adding up the the~$n + 2$ results of these multiplications and sprinkling in an alternating sign, we end up with~$d_n(m \tensor a_1 \tensor \dotsb \tensor a_n)$.
\end{remark}


