\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesPackage{customtex}

%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%   PACKAGES   %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%

% to patch commands
\RequirePackage{etoolbox}

% better typography
\RequirePackage{microtype}

% bibliography
\RequirePackage[
	backend    = biber,
	style      = alphabetic,
	dateabbrev = false,
	urldate    = long
]{biblatex}

% better enumerates
\RequirePackage{enumitem}
% quotation marks
\RequirePackage{csquotes}
% better looking tables
\RequirePackage{booktabs}

% math stuff
\RequirePackage{mathtools}
% more arrows
\RequirePackage{extarrows}
% theorem-like environments
\RequirePackage{amsthm}
% commutative diagrams
\RequirePackage{tikz-cd}

% allows to change the math font
% also loads fontspec
\RequirePackage[
	% no space around equal sign!
	warnings-off={mathtools-colon,mathtools-overbracket},
	partial = upright
]{unicode-math}

% detecting language
\RequirePackage{iflang}

% referencing and hyperlinking
% load hyperref after everything else
% except for cleveref
\RequirePackage{hyperref}
\RequirePackage[
	capitalise,
	noabbrev
]{cleveref}





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%   PACKAGE CONFIGURATION   %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%% FONT CONFIGURATION %%%%%

\setmainfont[
	Numbers = OldStyle,
	Contextuals = Alternate
]{Libertinus Serif}
\setsansfont[
	Contextuals = Alternate
]{Libertinus Sans}
\setmathfont{Libertinus Math}

% sanf serif for hyperlinks
\urlstyle{sf}

% adjust arrows in tikz to font
\RequirePackage{libertikz}



%%%%% SPACING CONFIGURATION %%%%%

\frenchspacing
\binoppenalty = \maxdimen % breaking of binary operators
\relpenalty   = \maxdimen % breaking of relation symbols

% show overfull hboxes
\overfullrule = 3mm

% fix a bug in amsmath
% see https://tex.stackexchange.com/questions/461537/
\patchcmd{\colon}{{:}}{\mathopen:}{}{}



%%%%% BIBLIOGRAPHY CONFIGURATION %%%%%

% always insert pp. after pagetotal,
% even if it isn’t recognized as a number by biblatex
\DeclareFieldFormat[book]{pagetotal}{#1~\ppno}

% no unplaned surprises with \cite
\DeclareFieldFormat{postnote}{#1}
\DeclareFieldFormat{multipostnote}{#1}

% formatting of entries
\DeclareFieldFormat[article,book,collection,incollection,proceedings,inproceedings]{number}{\mkbibbold{#1}}
\DeclareFieldFormat[article,collection,incollection]{volume}{\mkbibbold{#1}}
\DeclareFieldFormat[book]{pagetotal}{#1~\ppno}



%%%%%% LIST CONFIGURATION %%%%%

% enumerate; default is unindented
\setlist[enumerate]{
	wide          = 0pt,
	leftmargin    = *,
	listparindent = \parindent,
	parsep        = 0pt
}
\setlist[enumerate, 1]{
	label = {\arabic*.},
	ref   = {\arabic*}
}
\setlist[enumerate, 2]{
	label = {\alph*.},
	ref   = {\alph*}
}

% itemize; default is unindented
\setlist[itemize]{
	wide          = 0pt,
	leftmargin    = *,
	listparindent = \parindent,
	parsep        = 0pt
}
\setlist[itemize, 1]{ label = {\textbullet} }
\setlist[itemize, 2]{ label = {\textopenbullet} }

% description; default is unindented
\setlist[description, 1]{
	wide          = 0pt,
	listparindent = \parindent,
	parsep        = 0pt,
	format        = {\addperiod}
}
\setlist[description, 2]{
	leftmargin    = \parindent,
	labelindent   = \parindent,
	listparindent = \parindent,
	parsep        = 0pt,
	format        = {\normalfont\itshape\/\addperiod}
}

% enumerate*; an indented version of enumerate
\newlist{enumerate*}{enumerate}{2}
\setlist[enumerate*]{
	wide          = 0pt,
	leftmargin    = *,
	labelindent   = \parindent,
	listparindent = \parindent,
	parsep        = 0pt
}
\setlist[enumerate*, 1]{
	label = {\arabic*.},
 	ref   = {\arabic*}
}
\setlist[enumerate*, 2]{
	label = {\alph*.},
 	ref   = {\alph*}
}

% itemize*; an indented version of itemize
\newlist{itemize*}{itemize}{2}
\setlist[itemize*]{
	wide          = 0pt,
	leftmargin    = *,
	labelindent   = \parindent,
	listparindent = \parindent,
	parsep        = 0pt
}
\setlist[itemize*, 1]{  label = {\textbullet} }
\setlist[itemize*, 2]{  label = {\textopenbullet} }

% description*; an indented version of description
\newlist{description*}{description}{2}
\setlist[description*]{
	leftmargin    = \parindent,
	labelindent   = \parindent,
	listparindent = \parindent,
	parsep        = 0pt
}

% equivalenceslist for stating equivalences;
% unindented by default
\newlist{equivalenceslist}{enumerate}{2}
\setlist[equivalenceslist]{
	wide          = 0pt,
	leftmargin    = *,
	listparindent = \parindent,
	parsep        = 0pt
}
\setlist[equivalenceslist,1]{
	label = {\roman*.},
	ref   = {\roman*}
}
\setlist[equivalenceslist,2]{
	label = {\alph*.},
	ref   = {\alph*}
}

% equivalenceslist*; an indented version of equivalenceslist
\newlist{equivalenceslist*}{enumerate}{2}
\setlist[equivalenceslist*]{
	wide          = 0pt,
	leftmargin    = *,
	labelindent   = \parindent,
	listparindent = \parindent,
	parsep        = 0pt
}
\setlist[equivalenceslist*,1]{
	label = {\roman*.},
	ref   = {\roman*}
}
\setlist[equivalenceslist*,2]{
	label = {\alph*.},
 	ref   = {\alph*}
}

% implicationslist; for lists of implications in proofs
% never indented
\newlist{implicationslist}{description}{2}
\setlist[implicationslist]{
	wide          = 0pt,
	before        = {\renewcommand{\makelabel}{\dotlabel}},
	listparindent = \parindent,
	parsep        = 0pt
}

% shortcuts for the items in implicationslist
\newcommand{\addperiod}[1]{#1.}
\newcommand{\dotlabel}[1]{\normalfont #1.}
\newcommand{\textimp}[2]{\ref{#1}~$\implies$~\ref{#2}}
\newcommand{\textiff}[2]{\ref{#1}~$\iff$~\ref{#2}}
\newcommand{\impitem}[2]{\item[\textimp{#1}{#2}]}
\newcommand{\iffitem}[2]{\item[\textiff{#1}{#2}]}

% casedistinction; list for case distinctions
% never indented
\newlist{casedistinction}{enumerate}{2}
\setlist[casedistinction, 1]{
	wide          = 0pt,
	leftmargin    = *,
	listparindent = \parindent,
	parsep        = 0pt,
	label         = {\bfseries Case~\arabic*.},
	ref           = {\arabic*}
}
\setlist[casedistinction, 2]{
	wide          = 0pt,
	ref           = {\arabic*},
	label         = {\bfseries Case~\arabic{casedistinctioni}.\alph*.},
	ref           = {\arabic*}
}

\crefname{casedistinctioni}{Case}{Cases}
\Crefname{casedistinctioni}{Case}{Cases}



%%%%% THEOREM-LIKE ENVIRONMENTS CONFIGURATION %%%%%

\newcounter{everything}[section]

\theoremstyle{definition}

\newtheorem{construction}[everything]{\IfLanguageName{ngerman}{Konstruktion}{Construction}}
\newtheorem{convention}  [everything]{\IfLanguageName{ngerman}{Konvention}{Convention}}
\newtheorem{corollary}   [everything]{\IfLanguageName{ngerman}{Korollar}{Corollary}}
\newtheorem{definition}  [everything]{Definition}
\newtheorem{example}     [everything]{\IfLanguageName{ngerman}{Beispiel}{Example}}
\newtheorem{fluff}       [everything]{}
\newtheorem{lemma}       [everything]{Lemma}
\newtheorem{notation}    [everything]{Notation}
\newtheorem{proposition} [everything]{Proposition}
\newtheorem{question}    [everything]{\IfLanguageName{ngerman}{Frage}{Question}}
\newtheorem{recall}      [everything]{Recall}
\newtheorem{remark}      [everything]{\IfLanguageName{ngerman}{Bemerkung}{Remark}}
\newtheorem{theorem}     [everything]{\IfLanguageName{ngerman}{Satz}{Theorem}}
\newtheorem{warning}     [everything]{\IfLanguageName{ngerman}{Warnung}{Warning}}

% the numbering for claims is local too the surrounding theorem-like environments;
% to be used in proofs
\newtheorem{claim}{Claim} % unnumbered version
% unnumbered version of claim
\newtheorem*{claim*}{Claim} % unnumbered version

% printing of counters
\renewcommand{\theeverything}{\thesection.\arabic{everything}}
\renewcommand{\theclaim}{\arabic{claim}}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%   TEXT FORMATTING   %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\defemph}{\textbf}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%   MATH CONFIGURATION   %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%% ARROWS %%%%%

\renewcommand{\to}{
	\mathchoice
	{\longrightarrow}
	{\rightarrow}
	{\rightarrow}
	{\rightarrow}
}
\newcommand{\from}{
	\mathchoice
	{\longleftarrow}
	{\leftarrow}
	{\leftarrow}
	{\leftarrow}
}
\AtBeginDocument{
	\let\oldmapsto\mapsto
	\renewcommand{\mapsto}{
		\mathchoice
		{\longmapsto}
		{\oldmapsto}
		{\oldmapsto}
		{\oldmapsto}
	}
}
\AtBeginDocument{
	\let\oldmapsfrom\mapsfrom
	\renewcommand{\mapsfrom}{
		\mathchoice
		{\longmapsfrom}
		{\oldmapsfrom}
		{\oldmapsfrom}
		{\oldmapsfrom}
	}
}
\newcommand{\onetoone}{
	\mathchoice
	{\longleftrightarrow}
	{\leftrightarrow}
	{\leftrightarrow}
	{\leftrightarrow}
}
\newcommand{\xto}[1]{
	\mathchoice
	{\xrightarrow{\enspace #1 \enspace}}
	{\xrightarrow{#1}}
	{\xrightarrow{#1}}
	{\xrightarrow{#1}}
}
\newcommand{\xfrom}[1]{
	\mathchoice
	{\xleftarrow{\enspace #1 \enspace}}
	{\xleftarrow{#1}}
	{\xleftarrow{#1}}
	{\xleftarrow{#1}}
}



%%%%% NUMBERS AND FIELDS %%%%%

\newcommand{\boldfont}{\mathbb}
\newcommand{\Natural}{\boldfont{N}}
\newcommand{\Integer}{\boldfont{Z}}
\newcommand{\Rational}{\boldfont{Q}}
\newcommand{\Real}{\boldfont{R}}
\newcommand{\Complex}{\boldfont{C}}
\newcommand{\Finite}{\boldfont{F}}
\newcommand{\kf}{\boldfont{k}}
\newcommand{\Kf}{\boldfont{K}}
\newcommand{\Lf}{\boldfont{L}}



%%%%% OPERATORS %%%%%

\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\Center}{Z}
\DeclareMathOperator{\End}{End}
\DeclareMathOperator{\Ext}{Ext}
\DeclareMathOperator{\Exterior}{\bigwedge}
\DeclareMathOperator{\GL}{GL}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\Mat}{Mat}
\DeclareMathOperator{\SL}{SL}
\DeclareMathOperator{\Symm}{S}
\DeclareMathOperator{\Tor}{Tor}



%%%%% FUNCTIONS %%%%%

\DeclareMathOperator{\ringchar}{char}
\newcommand{\id}{\mathrm{id}}
\newcommand{\sign}{sgn}
\newcommand{\tr}{tr}



%%%%% SPECIAL ELEMENTS %%%%%

\newcommand{\Id}{\mathbb{1}}



%%%%% BINARY OPERATIONS %%%%%

\newcommand{\act}{\mathbin{.}}
\newcommand{\tensor}{\otimes}



%%%%% RELATIONS %%%%%

\newcommand{\defined}{\coloneqq}
\newcommand{\defines}{\eqqcolon}
\newcommand{\leftadjoint}{\dashv}
\newcommand{\rightadjoint}{\vdash}
\renewcommand{\setminus}{\smallsetminus}



%%%%% DELIMITERS %%%%%

\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclarePairedDelimiter{\class}{\lbrack}{\rbrack}
\DeclarePairedDelimiter{\ideal}{\lparen}{\rparen}
\DeclarePairedDelimiter{\norm}{\lVert}{\rVert}



%%%%% DECORATIONS %%%%%

\newcommand{\conj}{\overline}
\newcommand{\induced}{\overline}
\newcommand{\op}{\mathrm{op}}
\newcommand{\trans}{\mathsf{t}}




%%%%% MISC %%%%%

% iddots
\newcommand{\iddots}{⋰}

% larger bullet
\let\oldbullet\bullet
\renewcommand{\bullet}{{\scalebox{1.15}{\oldbullet}}}

% restrict{f}{X} for f|_X
% restrict[Y]{f}{X} for f
% stared version scales the delimiter
% restrict[\(bB)ig(g)] for manual scaling of the delimiter
% * has precedent over manual scaling if both are given
\DeclarePairedDelimiter{\prerestrict}{.}{\rvert} % period = empty delimiter
\NewDocumentCommand%
{\restrict}%
{s o m m O{}}%
{%
	\IfBooleanTF{#1}
	{%
		\prerestrict*{#3}_{#4}^{5}
	}
	{%
		\IfValueTF{#2}
		{%
			\prerestrict[#2]{#3}_{#4}^{5}
		}
		{%
			#3\rvert_{#4}^{5}
		}
	}
}

% \suchthat for inline
% \suchthat* for for display math with \left and \right
% \suchthat[\(bB)ig(g)] for manual scaling in display math
\NewDocumentCommand%
{\suchthat}%
{so}
{%
	\IfValueTF{#2}
	{%
		\:#2|\:
	}
	{%
		\IfBooleanTF{#1}
		{%
			\:\middle|\:
		}
		{%
			\mathrel{|}
		}
	}
}

